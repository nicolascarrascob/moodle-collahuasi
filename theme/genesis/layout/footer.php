<div id="footer" class="row">
    <div class="sklt-container">
        <div class="five columns alpha">
            <?php echo $OUTPUT->footermod("module1"); ?>
        </div> 
        <div class="six columns center-footer-module">
            <?php echo $OUTPUT->footermod("module2"); ?>
        </div>
        <div class="five columns omega float-right">
            <?php echo $OUTPUT->footermod("module3"); ?>
        </div>
    </div>
</div>
<div id="footerend" class="row">
    <div class="sklt-container">
        <div class="seven columns alpha">
            <div id="footerendleft">
                <?php echo $OUTPUT->copyright(); ?>
            </div>
        </div>
        <div class="nine columns omega float-right">
            <div id="footerendright">
                <?php echo $OUTPUT->socialicons('footer'); ?>
            </div>
        </div>
    </div>
</div>