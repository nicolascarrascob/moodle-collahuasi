<?php

/**
 * Makes our changes to the CSS
 *
 * @param string $css
 * @param theme_config $theme
 * @return string 
 */
function genesis_process_css($css, $theme) {
    global $CFG;
    $themecolor = $theme->settings->themecolor;
    
    switch ($themecolor) {
        case 'blue':
            $color1 = "#00A5B6";
            $color2 = "#003050";
            $color3 = "#0094A3";
            $color4 = "#CADD09";
            $themespriteposition = "-25px";
            break;
        case 'green':
            $color1 = "#5DBB71";
            $color2 = "#27736F";
            $color3 = "#4F9F60";
            $color4 = "#EDDC2A";
            $themespriteposition = "-50px";
            break;
        case 'orange':
            $color1 = "#D58303";
            $color2 = "#5F6366";
            $color3 = "#C28425";
            $color4 = "#FED060";
            $themespriteposition = "-75px";
            break;
    }
    
    $layoutStyle = $theme->settings->layoutStyle;
    $bgstyle = '';

    switch ($layoutStyle) {
        case 'bgcolor':
            $bgstyle = '
                        body{
                            left: 50%;
                            margin-left: -550px;
                            position: absolute;
                            width: 1100px;
                        }

                        html{
                            background: '.$theme->settings->bgcolor.'
                        }

                        ';
            break;
        case 'bgpattern':
            $bgstyle = '
                        body{
                            left: 50%;
                            margin-left: -550px;
                            position: absolute;
                            width: 1100px;
                        }

                        html{
                            background-image: url('.((isset($theme->settings->bgpatternCustom) && trim($theme->settings->bgpatternCustom) != "")?$theme->settings->bgpatternCustom:$CFG->wwwroot."/theme/genesis/pix/patterns/".$theme->settings->bgpattern).');
                            background-attachment: scroll;
                            background-size: auto;
                            background-position: 50% 50%;
                            background-repeat: repeat repeat;
                        }

                        ';
            break;
        case 'bgimage':
            $bgstyle = '
                        body{
                            left: 50%;
                            margin-left: -550px;
                            position: absolute;
                            width: 1100px;
                        }

                        html{
                            background-image: url('.$theme->settings->bgimage.');
                            background-attachment: fixed;
                            background-size: cover;
                            background-position: 50% 50%;
                            background-repeat: no-repeat no-repeat;
                        }

                        ';
            break;
    }

    

    $css = str_replace("[[setting:color1]]", $color1, $css);
    $css = str_replace("[[setting:color2]]", $color2, $css);
    $css = str_replace("[[setting:color3]]", $color3, $css);
    $css = str_replace("[[setting:color4]]", $color4, $css);
    $css = str_replace("[[setting:themespriteposition]]", $themespriteposition, $css);
    $css = str_replace("[[setting:logoHeight]]", $theme->settings->logoHeight.'px', $css);
    $css = str_replace("[[setting:headerPadding]]", $theme->settings->headerPadding.'px', $css);
    $css = str_replace("[[setting:menuMarginTop]]", $theme->settings->menuMarginTop.'px', $css);
    $css = str_replace("[[setting:footermod_aboutus_whitelogo]]", $theme->settings->footermod_aboutus_whitelogo, $css);

    $css = str_replace("[[setting:bgstyle]]", $bgstyle, $css);

    // Return the CSS
    return $css;
}

?>