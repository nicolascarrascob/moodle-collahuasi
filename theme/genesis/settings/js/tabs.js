$(function(){
	$(".sidebarTabs").click(function(){
		var _content = $(this).attr('id').replace("Tab","Area");
		$(".sidebarTabs").removeClass('tabActive');
		$(".settingsArea").css('display','none');
		$(this).addClass('tabActive');
		$("#" + _content).fadeIn();
	});
});